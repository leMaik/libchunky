Copyright (c) 2013-2016 Wertarbyte (https://wertarbyte.com)

Permission to modify and redistribute is granted under the terms of the GPLv3 license. Read LICENSE.txt for the full
license.

libchunky uses the awesome Minecraft pathtracer Chunky by Jesper Öqvist. Chunky is covered by the GPL v3 license.
