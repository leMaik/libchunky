/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import se.llbit.chunky.renderer.RenderStatusListener;
import se.llbit.util.ProgressListener;

/**
 * Wrapper for {@link RenderStatusListener}.
 */
class RenderStatusListenerWrapper implements ProgressListener {
    private final RenderListener listener;

    RenderStatusListenerWrapper(RenderListener listener) {
        this.listener = listener;
    }

    @Override
    public void setProgress(String task, int done, int start, int target) {
        if (task.equalsIgnoreCase("rendering")) {
            listener.onRenderStatusChanged(done, target);
        } else {
            listener.onStatusChanged(ChunkyTask.fromString(task));
        }
    }

    @Override
    public void setProgress(String task, int done, int start, int target, String eta) {
        if (task.equalsIgnoreCase("rendering")) {
            listener.onRenderStatusChanged(done, target);
        } else {
            listener.onStatusChanged(ChunkyTask.fromString(task));
        }
    }
}
