/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import se.llbit.chunky.renderer.scene.Scene;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * A wrapper for chunky.
 */
public interface ChunkyWrapper extends UnmodifiableChunkyWrapper {
    /**
     * Sets the scene directory for this chunky instance. This must be done before rendering.
     *
     * @param sceneDirectory scene directory
     */
    void setSceneDirectory(File sceneDirectory);

    /**
     * Gets the scene for this chunky instance.
     *
     * @return scene
     */
    Scene getScene();

    /**
     * Sets the scene for this chunky instance.
     *
     * @param scene scene
     */
    void setScene(Scene scene) throws IOException;

    /**
     * Sets the texturepack for this chunky instance. This can only be done before rendering.
     *
     * @param texturepack texturepack to use
     * @throws IOException if loading the texturepack fails
     */
    void setTexturepack(File texturepack);

    /**
     * Starts the wrapped chunky instance.
     *
     * @throws IOException if an error occures while rendering
     */
    void render() throws IOException;

    /**
     * Stops the wrapped chunky instance.
     */
    void stop();

    /**
     * Gets the latest available image.
     *
     * @return latest available image or null, if none is available
     * @throws IOException if getting the image fails
     */
    BufferedImage getImage() throws IOException;

    /**
     * Gets the latest available render dump.
     *
     * @return latest available render dump or null, if none is available
     * @throws IOException if getting the render dump fails
     */
    ChunkyRenderDump getDump() throws IOException;

    /**
     * Adds the given listener.
     *
     * @param listener listener to add
     */
    void addListener(RenderListener listener);

    /**
     * Removes the given listener.
     *
     * @param listener listener to remove
     */
    void removeListener(RenderListener listener);

    /**
     * Sets the target samples per second.
     *
     * @param targetSpp target samples per second
     */
    void setTargetSpp(int targetSpp);

    /**
     * Sets the number of threads to use for rendering.
     *
     * @param threadCount number of rendering threads
     */
    void setThreadCount(int threadCount);
}
