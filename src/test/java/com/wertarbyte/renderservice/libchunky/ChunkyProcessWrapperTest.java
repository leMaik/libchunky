/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.wertarbyte.renderservice.libchunky.util.FileUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import se.llbit.chunky.renderer.scene.Scene;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ChunkyProcessWrapperTest {
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private ChunkyProcessWrapper chunky;
    private File sceneDirectory;
    private Scene scene;

    @Before
    public void setup() throws Exception {
        sceneDirectory = temporaryFolder.newFolder();

        try (InputStream in = getClass().getClassLoader().getResourceAsStream("testscene/test_scene.json")) {
            scene = new Scene();
            scene.loadDescription(in);
        }

        //extract scene files
        FileUtil.extractResource("testscene/test_scene.foliage", new File(sceneDirectory, "test_scene.foliage"));
        FileUtil.extractResource("testscene/test_scene.grass", new File(sceneDirectory, "test_scene.grass"));
        FileUtil.extractResource("testscene/test_scene.octree", new File(sceneDirectory, "test_scene.octree"));

        chunky = new ChunkyProcessWrapper();
        chunky.setJvmMinMemory(512);
        chunky.setJvmMaxMemory(2048);
        chunky.setTargetSpp(1);
        chunky.setThreadCount(2);
        chunky.setSceneDirectory(sceneDirectory);
        chunky.setScene(scene);
    }

    @After
    public void tearDown() {
        chunky.stop();
    }

    @Test
    public void testImageCreation() throws Exception {
        chunky.render();

        assertThat(chunky.getImage(), is(notNullValue()));
        assertThat(chunky.getImage().getWidth(), is(scene.width));
        assertThat(chunky.getImage().getHeight(), is(scene.height));
    }

    @Test
    public void testDumpCreation() throws Exception {
        chunky.render();

        assertThat(chunky.getDump(), is(notNullValue()));
        assertThat(chunky.getDump().getWidth(), is(scene.width));
        assertThat(chunky.getDump().getHeight(), is(scene.height));
        assertThat(chunky.getDump().getSpp(), is(1));
    }

    @Test
    public void testListeners() throws Exception {
        final AtomicBoolean finished = new AtomicBoolean(false);
        final AtomicBoolean finishedExactlyOnce = new AtomicBoolean(false);
        final AtomicBoolean statusChangedWhenFinished = new AtomicBoolean(false);

        chunky.addListener(new RenderListener() {
            @Override
            public void onFinished() {
                if (finished.get()) {
                    finishedExactlyOnce.set(false);
                } else {
                    finishedExactlyOnce.set(true);
                }
                finished.set(true);
            }

            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                if (currentSpp == targetSpp) {
                    statusChangedWhenFinished.set(true);
                }
            }

            @Override
            public void onStatusChanged(ChunkyTask task) {

            }
        });
        chunky.render();

        assertThat(finished.get(), is(true));
        assertThat(finishedExactlyOnce.get(), is(true));
        assertThat(statusChangedWhenFinished.get(), is(true));
    }

    @Test
    public void testChunkyCrash() throws Exception {
        chunky.setJvmMaxMemory(0); //make chunky crash
        boolean exceptionThrown = false;
        try {
            chunky.render();
        } catch (IOException e) {
            exceptionThrown = true;
        }

        assertThat("IOException should be thrown if chunky crashes", exceptionThrown, is(true));
    }

    @Test
    public void testMissingTexturepack() throws Exception {
        chunky.setTexturepack(new File("this does not exist"));

        exception.expect(IOException.class);
        chunky.render();
    }

    @Test
    public void testPathsWithWhitespace() throws Exception {
        sceneDirectory = new File(sceneDirectory, "directory with spaces");
        sceneDirectory.mkdir();

        FileUtil.extractResource("testscene/test_scene.foliage", new File(sceneDirectory, "test_scene.foliage"));
        FileUtil.extractResource("testscene/test_scene.grass", new File(sceneDirectory, "test_scene.grass"));
        FileUtil.extractResource("testscene/test_scene.octree", new File(sceneDirectory, "test_scene.octree"));
        chunky.setSceneDirectory(sceneDirectory);

        final AtomicBoolean running = new AtomicBoolean(false);
        chunky.addListener(new RenderListenerAdapter() {
            @Override
            public void onRenderStatusChanged(int currentSpp, int targetSpp) {
                running.set(true);
            }
        });
        chunky.render();
        assertTrue("Chunky should not crash if the paths contain spaces", running.get());
    }
}