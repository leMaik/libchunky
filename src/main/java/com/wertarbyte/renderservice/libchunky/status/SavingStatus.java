/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.status;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SavingStatus extends ChunkyStatus {
    private static final Pattern SAVING = Pattern.compile("Saving (.+?): (\\d+[\\.,]\\d+)% .*");

    private double progress;
    private Element savingElement;

    public SavingStatus(String line, Element element, double progress) {
        super(line);

        this.progress = progress;
        this.savingElement = element;
    }

    @Override
    public double getProgressPercentage() {
        return progress;
    }

    @Override
    public String toString() {
        return String.format("Saving %s: %f%%", savingElement.toString(), getProgressPercentage());
    }

    public Element getSavingElement() {
        return savingElement;
    }

    /**
     * Parses a line from chunky log to an instance of this class.
     *
     * @param line line from chunky
     * @return either a LoadingStatus or null if parsing failed (or this is not a loading status)
     */
    public static SavingStatus parse(String line) {
        DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
        format.setParseBigDecimal(true);

        Matcher m = SAVING.matcher(line);
        if (m.find()) {
            Element savingElement;

            String type = m.group(1);
            if (type.equals("render dump"))
                savingElement = Element.RenderDump;
            else
                savingElement = Element.Unknown;
            try {
                return new SavingStatus(line, savingElement, format.parse(m.group(2)).doubleValue());
            } catch (ParseException e) {
                return null;
            }
        }
        return null;
    }

    public enum Element {
        Unknown, RenderDump
    }
}
