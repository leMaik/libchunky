/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.util;

import java.io.*;

/**
 * Contains static file utility methods.
 */
public class FileUtil {
    /**
     * Writes the given stream into a file.
     *
     * @param source stream to write down
     * @param dest   target file
     * @throws IOException if an error occured while writing the file or reading the stream
     */
    public static void writeFile(InputStream source, File dest) throws IOException {
        try (OutputStream destStream = new FileOutputStream(dest)) {
            try {
                byte[] buffer = new byte[1024];
                int length;
                while ((length = source.read(buffer)) > 0) {
                    destStream.write(buffer, 0, length);
                }
            } finally {
                if (source != null)
                    source.close();
            }
        }
    }

    /**
     * Extracts the test resource with the given name.
     *
     * @param resource name of a test resource
     * @param dest     destination file
     * @throws IOException if extracting the file failed
     */
    public static void extractResource(String resource, File dest) throws IOException {
        writeFile(FileUtil.class.getClassLoader().getResourceAsStream(resource), dest);
    }

    /**
     * Deletes the given directory recursively.
     *
     * @param directory directory to delete
     * @return total number of files that were removed
     */
    public static int deleteDirectory(File directory) {
        int count = 0;
        File[] contents = directory.listFiles();
        if (contents != null) {
            for (File f : contents) {
                count += deleteDirectory(f);
            }
        }
        if (directory.delete()) {
            count++;
        }
        return count;
    }
}
