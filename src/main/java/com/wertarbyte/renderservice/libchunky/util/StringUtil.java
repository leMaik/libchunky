/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.util;

/**
 * String utility methods.
 */
public class StringUtil {
    public static String join(Iterable<String> array, String delimeter) {
        StringBuilder builder = new StringBuilder();
        for (String s : array) {
            builder.append(s).append(delimeter);
        }
        return builder.substring(0, Math.max(0, builder.length() - delimeter.length()));
    }
}
