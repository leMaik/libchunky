# libchunky
This library wraps [Chunky](http://chunky.llbit.se) as a convenient library to
be used in other applications.

## Usage
_Coming soon_

## Versioning
The version will depend on the bundled Chunky version, e.g. 1.3.7-SNAPSHOT uses Chunky 1.3.7 for rendering. Note that the API and the scene description format may change across versions.
