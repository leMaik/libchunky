/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.util;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * Builder for classpaths.
 */
public class ClasspathBuilder {
    /**
     * Builds the classpath for the given classes. Duplicate paths are only included once.
     *
     * @param classes classes
     * @return classpath for the given classes, seperated by {@link java.io.File#pathSeparator}
     */
    public static String buildClasspath(Class... classes) {
        Set<String> paths = new HashSet<>();

        // Add the class path of classes in jars that chunky needs:
        for (Class clazz : classes) {
            paths.add(clazz.getProtectionDomain().getCodeSource().getLocation().getFile().replace("%20", " "));
        }

        StringBuilder classPath = new StringBuilder();
        for (String path : paths) {
            classPath
                    .append(path)
                    .append(File.pathSeparator);
        }

        return classPath.substring(0, classPath.length() - File.pathSeparator.length());
    }
}
