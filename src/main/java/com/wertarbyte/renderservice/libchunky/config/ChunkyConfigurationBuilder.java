/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky.config;

import java.io.File;

/**
 * A builder for {@link ChunkyConfiguration}s.
 */
public class ChunkyConfigurationBuilder {
    private int jvmMaxMemory;
    private int jvmMinMemory;
    private File defaultTexturePack;
    private File sceneDirectory;
    private String sceneName;
    private int targetSpp;
    private int threadCount;
    private int dumpFrequency;

    ChunkyConfigurationBuilder() {
    }

    public ChunkyConfigurationBuilder setJvmMaxMemory(int jvmMaxMemory) {
        this.jvmMaxMemory = jvmMaxMemory;
        return this;
    }

    public ChunkyConfigurationBuilder setJvmMinMemory(int jvmMinMemory) {
        this.jvmMinMemory = jvmMinMemory;
        return this;
    }

    public ChunkyConfigurationBuilder setDefaultTexturePack(File texturePack) {
        this.defaultTexturePack = texturePack;
        return this;
    }

    public ChunkyConfigurationBuilder setSceneDirectory(File sceneDirectory) {
        this.sceneDirectory = sceneDirectory;
        return this;
    }

    public ChunkyConfigurationBuilder setSceneName(String sceneName) {
        this.sceneName = sceneName;
        return this;
    }

    public ChunkyConfigurationBuilder setTargetSpp(int targetSpp) {
        this.targetSpp = targetSpp;
        return this;
    }

    public ChunkyConfigurationBuilder setThreadCount(int threadCount) {
        this.threadCount = threadCount;
        return this;
    }

    public ChunkyConfigurationBuilder setDumpFrequency(int dumpFrequency) {
        this.dumpFrequency = dumpFrequency;
        return this;
    }

    public ChunkyConfiguration build() {
        return new ChunkyConfiguration(
                jvmMaxMemory, jvmMinMemory,
                defaultTexturePack,
                sceneDirectory, sceneName,
                targetSpp, threadCount, dumpFrequency);
    }
}
