/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import com.wertarbyte.renderservice.libchunky.status.RenderStatus;

/**
 * A chunky wrapper that allows getting properties of a chunky instance but can't modify anything.
 */
public interface UnmodifiableChunkyWrapper {
    /**
     * Gets the current samples per second.
     *
     * @return current samples per second
     */
    double getSamplesPerSecond();

    /**
     * Gets the number of rendering threads.
     *
     * @return number of rendering threads
     */
    int getThreadCount();

    /**
     * Gets the current status.
     *
     * @return status
     */
    RenderStatus getStatus();

    /**
     * Gets the previous status.
     *
     * @return previous status
     */
    RenderStatus getPreviousStatus();
}
