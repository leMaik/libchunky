/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

import java.io.File;

/**
 * A configuration for a chunky instance.
 */
public class ChunkyProcessConfig {
    private final int jvmMaxMemory;
    private final int jvmMinMemory;
    private final File chunkyJar;
    private final File chunkyLibraryDirectory;
    private final File defaultTexturePack;
    private final File sceneDirectory;
    private final String sceneName;
    private final int targetSpp;
    private final int threadCount;
    private final int dumpFrequency;

    ChunkyProcessConfig(int jvmMaxMemory, int jvmMinMemory, File chunkyJar, File chunkyLibraryDirectory,
                        File defaultTexturePack, File sceneDirectory,
                        String sceneName, int targetSpp, int threadCount, int dumpFrequency) {
        this.jvmMaxMemory = jvmMaxMemory;
        this.jvmMinMemory = jvmMinMemory;
        this.chunkyJar = chunkyJar;
        this.chunkyLibraryDirectory = chunkyLibraryDirectory;
        this.defaultTexturePack = defaultTexturePack;
        this.sceneDirectory = sceneDirectory;
        this.sceneName = sceneName;
        this.targetSpp = targetSpp;
        this.threadCount = threadCount;
        this.dumpFrequency = dumpFrequency;
    }

    public static ChunkyProcessConfigurationBuilder builder() {
        return new ChunkyProcessConfigurationBuilder();
    }

    public int getJvmMaxMemory() {
        return jvmMaxMemory;
    }

    public int getJvmMinMemory() {
        return jvmMinMemory;
    }

    public File getChunkyJar() {
        return chunkyJar;
    }

    public File getChunkyLibraryDirectory() {
        return chunkyLibraryDirectory;
    }

    public File getDefaultTexturePack() {
        return defaultTexturePack;
    }

    public File getSceneDirectory() {
        return sceneDirectory;
    }

    public File getSceneFile() {
        return new File(getSceneDirectory(), sceneName + ".json");
    }

    public String getSceneName() {
        return sceneName;
    }

    public int getTargetSpp() {
        return targetSpp;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public int getDumpFrequency() {
        return dumpFrequency;
    }
}
