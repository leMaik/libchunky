/*
 * Copyright (c) 2013-2016 Wertarbyte <https://wertarbyte.com>
 *
 * This file is part of libchunky.
 *
 * libchunky is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libchunky is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libchunky.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.wertarbyte.renderservice.libchunky;

/**
 * Tasks chunky performs while rendering.
 */
public enum ChunkyTask {
    RENDERING,
    LOADING,
    SAVING,
    WRITING_PNG,
    UNKNOWN;

    static ChunkyTask fromString(String task) {
        if (task.equalsIgnoreCase("rendering")) {
            return RENDERING;
        } else if (task.startsWith("Loading")) {
            return LOADING;
        } else if (task.startsWith("Saving")) {
            return SAVING;
        } else if (task.equalsIgnoreCase("writing png")) {
            return WRITING_PNG;
        }
        return UNKNOWN;
    }
}
